
# Getting started with Vue  

## **Setup your working environment** 
The setup described here is quite commonly used, and you can get a lot of additional support online on all of the tools used. You can look at this video showing a lot of [learning resources](https://www.youtube.com/watch?v=QGy6M8HZSC4)

Some Vue related YouTube videos are:
- [Vue JS Crash Course - 2019](https://www.youtube.com/watch?v=Wy9q22isx3U&t=2446s)
- [FULL PROJECT with Vue.js + Vuetify + Firebase](https://www.youtube.com/watch?v=FXY1UyQfSFw&list=PL55RiY5tL51qxUbODJG9cgrsVd7ZHbPrt) 

### Online code palyground alternatives
If you want to try/share some code really quick without going throug the setup steps below you can use some of the following websites:
 - [Jsfiddle](https://jsfiddle.net/boilerplate/vue) 
 - [CodeSandbox](https://codesandbox.io/s/vue)
 - [CodePen](https://codepen.io/)


## Install Visual Studio Code 
Visual Studio Code is a lightweight **code editor** which you can dowload from https://code.visualstudio.com/

other free alternatives are [Atom](https://atom.io/), [Notepad++](https://notepad-plus-plus.org/), [Eclipse](https://www.eclipse.org/) 

## Install Git
Git is **version control system** which allows you to store version of your code and collaborate with other contributors to the project. 
Install it from: https://git-scm.com/download/win

During Git setup select 
 - Choose git from the command line and also from 3-th party software
   
from the options in the "Adjusting your PATH environment" setup screen

## Install Node.js 
Node.js is **JavaScript run-time environment** that executes JavaScript code outside of a browser.
To install got to https://nodejs.org/en/

## Install Vue 
Vue.js is one of the most popular JavaScript **frameworks for building user interfaces** and single-page applications. To install use the following command:

`npm install -g @vue/cli`

Other popular frameworks are [Angular](https://angular.io/) and [React](https://reactjs.org/) 


## Create your GitLab account 
We will use Gitlab as **Git-repository manager** (server on which we store the all versions of the source code), however it has additional functionality allowing easier integration and depolyment of your applications. 

Register your user at: https://about.gitlab.com/

## **Working on your project**

## Fork this template
You can start working on your project by forking (make a copy) of this project in your own repository.  

## Clone the project to your local machine 
Clone the project form your repository on your local machine where you can start working on it. To do so:
1.  Go to the main page of your freshly forked project and look at the top right corner for the button clone. 
2.  Choose the second option (Clone with HTTPS) and copy the link to the clipboard
3.  Open command prompt and go to the directory you want to sotre your projects
4.  Type 
`git clone`
and paste the path to your repository. At the end the command should look something like this 
```
git clone https://gitlab.com/Aladjov/hyper-vue.git
``` 
5.  Change directory to the newly downloaded project 
```
cd hyper-vue
``` 
6.  Open the Visual Studio Code editor by typing 
```
code .
``` 
7.  From the menue open new Treminal window 

## Install all dependencies 
You need to download and install all dependencies of the project by running the command (in the terminal window)
```
npm install
```

## Run Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).