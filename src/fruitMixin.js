export const fruitMixin = {
   data(){
      return {
         text:'',
         fruits:['Apple','Orange', 'Bannana','Pineapple', 'Watermelon','Pear','Mango','Melon'], 
         filterText:''
      }
   }, 
   computed:{
      //better perfromance then using filters directly because vue knows when to update it and does not render the element each time the DOM is updated
      filteredFruits() {
         return this.fruits.filter((element)=>{
            return element.match(this.filterText);
         });
      }
   },
   created(){
      console.log('MixIn Created');
   }
};
