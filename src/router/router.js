import Vue from 'vue'
import VueRouter from 'vue-router';
import { routes } from './routes'

Vue.use(VueRouter);

export const router = new VueRouter({
   routes: routes,
   mode: 'history', //default mode is to use hash (/#/) in the url but history mode requires the server to be configured to always return index.html
   scrollBehavior (to, from, savedPosition) {
      if (savedPosition){
         return savedPosition
      }
      // return {x:0,y:1200}
      if (to.hash) {
         return ({selector: to.hash})
      }
   }
})

router.beforeEach((to, from, next) => {
   console.log(`Global beforeEach from:${from.path} to:${to.path}`);
   next(); 
   // next(false); // does not continue
   // next({path:'/'}) use redirection path 
})

