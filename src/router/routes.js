import Home from '../components/Home.vue'
import Header from '../components/Header.vue'
import {store} from "../store/store.js";

//Conditional Lazy loading recognized by Webpack 
//We want to load somehting only if we visit certain path 
// and resolve the asynchroneous function resolve 

const User = resolve =>{
  require.ensure(['../components/user/User.vue'], () => {
    resolve(require('../components/user/User.vue'))
  });
} 

//All users routes are combined in one bundle by using a group name: usersGroupName
//User is still separate bundle
const Users = resolve =>{
  require.ensure(['../components/user/Users.vue'], () => {
    resolve(require('../components/user/Users.vue'))
  },'usersGroupName');
} 
const UserStart = resolve =>{
  require.ensure(['../components/user/UserStart.vue'], () => {
    resolve(require('../components/user/UserStart.vue'))
  },'usersGroupName');
} 
const UserDetail = resolve =>{
  require.ensure(['../components/user/UserDetail.vue'], () => {
    resolve(require('../components/user/UserDetail.vue'))
  },'usersGroupName');
} 
const UserEdit = resolve =>{
  require.ensure(['../components/user/UserEdit.vue'], () => {
    resolve(require('../components/user/UserEdit.vue'))
  },'usersGroupName');
} 


import WelcomePage from '../components/welcome/welcome.vue'
import DashboardPage from '../components/dashboard/dashboard.vue'
import SignupPage from '../components/auth/signup.vue'
import SigninPage from '../components/auth/signin.vue'

export const routes = [
   {path:'',components:{
      default: Home, 
      'header-top': Header
   }},
   {path:'/user/:id',components:{
      default:User,
     'header-bottom':Header}},
   {path:'/users', components:{
      default:Users,
     'header-bottom':Header},
     children:[
      {path:'',component:UserStart},
      {path:':id',component:UserDetail, 
        beforeEnter: (to, from, next) => {
          console.log(`UserDetail beforeEach from:${from.path} to:${to.path}`);
          next();
        }},
      {path:':id/edit', name:'userEdit',component:UserEdit}
   ]},
   {path: '/redirect-me', redirect :'/users'},
   { path: '/axios', component: WelcomePage },
   { path: '/axios/signup', component: SignupPage },
   { path: '/axios/signin', component: SigninPage },
   { path: '/axios/dashboard', component: DashboardPage, 
      beforeEnter(to,from,next){
        console.log('idToken:'+store.getters.idToken)
          if(store.getters.idToken){
            next()
          }
          else
            next('/axios/signin')
      } },
   {path: '*', redirect: '/'}
];