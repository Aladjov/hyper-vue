import Vue from 'vue'
import VueResource from 'vue-resource';

export default function setupVueResources(){
   Vue.use(VueResource);
   Vue.http.options.root='https://vuejs-tester.firebaseio.com/';
   Vue.http.interceptors.push((request,next) => {
      if((request.url=="user.json")&&(request.method =='POST'))
         request.method='PUT'
      next(response =>{
         //this is where we can modify the response 
      })   
   })
}

