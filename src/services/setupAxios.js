import axios from 'axios';

export default function setupAxios(){
   axios.defaults.baseURL='https://vuejs-tester.firebaseio.com/';
 //  axios.defaults.headers.common['Authorization']='Some Key here';
 //  axios.defaults.headers.get['Accept']='application/json';
   const reqInterceptor = axios.interceptors.request.use((config)=>{
      console.log("Request interceptor")
      console.log(config);
      return config
   });
   const respInterceptor = axios.interceptors.response.use((response)=>{
      console.log("Response interceptor")
      console.log(response);
      return response
   });
   axios.interceptors.request.eject(reqInterceptor);
   axios.interceptors.response.eject(respInterceptor);
}