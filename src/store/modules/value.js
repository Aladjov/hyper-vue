const state={
   value: ''
};

const getters={
   value: state=>{
      return state.value;
   }
};

const mutations={
   setValue: (state,payload) => {
      state.value = payload;
   }
};

const actions={
   setValue:({commit},payload) => {
      commit('setValue',payload) 
   }
};

export default {
   state:state,
   getters, //using ES6 syntax to expand it automatically to getters:getters
   mutations, 
   actions
};